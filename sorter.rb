require "csv"

filename=	ARGV[0]
output=		ARGV[1]
columnFile=	ARGV[2]
columnNames=[]

if File.exist?(output) == false
	columnNames=	File.read(columnFile).split(/\n/).map{|el| el.strip}
	CSV.open(output,'w') do |csv|
		csv << columnNames
	end	
end

idFile=		ARGV[3]
File.open(idFile,'r').readlines[0..-1].each{|id|
	id=	id.strip

	# p id
	hsh=	{
		"id" => id
	}

	CSV.foreach(filename) do |row|
		if row[0]===id
			hsh[row[1].downcase] = row[2]
		end
	end

	dataArray=	Array.new(columnNames.length){|element| nil}	# Make array as long as `columnNames`, filled with `nil`
	hsh.each{|key,val|
		columnNames.each_with_index{|colName,idx|
			if key===colName
				dataArray.insert(idx,val)
			end
		}
	}

	if dataArray.compact.length > 1	# dataArray should have more than one element. If only one, it's the ID which would be in there anyway
		CSV.open(output,'a') do |csv|
			csv << dataArray
		end	
		p dataArray,"====="
	end
}