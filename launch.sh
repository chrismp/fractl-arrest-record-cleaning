#!/bin/bash

LOG_DIR=logs
mkdir -p $LOG_DIR	# Make directory if it does not exist

DATA_FILE_NAMES=(arrest_features features)

for DATA_FILE_NAME in ${DATA_FILE_NAMES[@]}; do
	# Launch Ruby script to make text file with column names in it
	CSV=$DATA_FILE_NAME.csv
	UNIQUE_ID_FILE=$DATA_FILE_NAME-IDs.txt
	COLUMN_NAME_FILE=$DATA_FILE_NAME-columns.txt
	nohup ruby columns_uniqueIDs_listmakers.rb $CSV $UNIQUE_ID_FILE $COLUMN_NAME_FILE > $LOG_DIR/$DATA_FILE_NAME-IDs.log 2>&1
	
	# Split big CSV file into chunks
	CHUNK_DIR=$DATA_FILE_NAME-chunks
	LINES_PER_CHUNK=10000
	mkdir -p $CHUNK_DIR
	split --additional-suffix=".csv" -l $LINES_PER_CHUNK $CSV $CHUNK_DIR/	# Split CSV into smaller CSVs of 10000 lines each, put those files in chunks directory
done
echo "Done splitting CSV files and making text files containing column names and IDs"

for DATA_FILE_NAME in ${DATA_FILE_NAMES[@]}; do
	for CHUNK_FILE_PATH in $DATA_FILE_NAME-chunks/*; do
		CHUNK_FILE_NAME=${CHUNK_FILE_PATH##*/}	# http://stackoverflow.com/questions/9011233/for-files-in-directory-only-echo-filename-no-path
		nohup ruby sorter.rb $CHUNK_FILE_PATH output-$DATA_FILE_NAME.csv $COLUMN_NAME_FILE $UNIQUE_ID_FILE > $LOG_DIR/$DATA_FILE_NAME-$CHUNK_FILE_NAME.log 2>&1 &
	done
done