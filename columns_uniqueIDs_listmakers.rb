require "csv"

csvFile=	ARGV[0]
idFile=		ARGV[1]
columnFile=	ARGV[2]

ids=		[]
columnNames=["id"]

CSV.foreach(csvFile) do |row|
	ids << row[0]

	potentialColumnName=	row[1]===nil ? '' : row[1].downcase
	if columnNames.include?(potentialColumnName) === false
		columnNames << potentialColumnName.strip
	end
end

File.open(columnFile,'w'){|file|
	columnNames.each{|col| file.puts(col)}
}

if File.exist?(idFile) == false
	File.open(idFile,'w'){|file|  
		ids.uniq.each{|elem|
			file.puts(elem)
		}
	}
end